Name:		perl-Test-XML
Version:	0.08
Release:	10%{?dist}
Summary:	Compare XML in perl tests
Group:		Development/Libraries
License:	GPL+ or Artistic
URL:		http://search.cpan.org/dist/Test-XML/
Source0:	http://search.cpan.org/CPAN/authors/id/S/SE/SEMANTICO/Test-XML-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(id -nu)
BuildArch:	noarch
# Build
BuildRequires:	perl-interpreter
BuildRequires:	perl-generators
BuildRequires:	perl(ExtUtils::MakeMaker)
# Module
BuildRequires:	perl(Carp)
BuildRequires:	perl(strict)
BuildRequires:	perl(Test::Builder)
BuildRequires:	perl(Test::More)
BuildRequires:	perl(warnings)
BuildRequires:	perl(XML::LibXML)
BuildRequires:	perl(XML::Parser)
BuildRequires:	perl(XML::SAX)
BuildRequires:	perl(XML::SAX::ParserFactory)
BuildRequires:	perl(XML::SAX::Writer)
BuildRequires:	perl(XML::SemanticDiff)
BuildRequires:	perl(XML::Twig)
BuildRequires:	perl(XML::XPath)
# Test Suite
BuildRequires:	perl(Test::Builder::Tester)
BuildRequires:	perl(XML::SAX::Base)
# Runtime
Requires:	perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))
# Only XML::LibXML is actually needed for Test::XML::XPath, but we require
# XML::XPath too in case someone wants to use Test::XML::XPath::XML::XPath
# directly for some reason
Requires:	perl(XML::LibXML)
Requires:	perl(XML::XPath)

%description
This module contains generic XML testing tools. Functions include:

is_xml(GOT, EXPECTED [, TESTNAME ])

  This function compares GOT and EXPECTED, both of which are strings of XML.
  The comparison works semantically and will ignore differences in syntax
  that are meaningless in xml, such as different quote characters for
  attributes, order of attributes or empty tag styles. It returns true or
  false, depending upon test success.

isnt_xml(GOT, MUST_NOT_BE [, TESTNAME ])

  This function is similiar to is_xml(), except that it will fail if GOT and
  MUST_NOT_BE are identical.

is_well_formed_xml(XML [, TESTNAME ])

  This function determines whether or not a given XML string is parseable as
  XML.

is_good_xml(XML [, TESTNAME ])

    This is an alias for is_well_formed_xml().

%prep
%setup -q -n Test-XML-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
%{_fixperms} %{buildroot}

%check
make test

%clean
rm -rf %{buildroot}

%files
%doc Changes README
%{perl_vendorlib}/Test/
%{_mandir}/man3/Test::XML.3*
%{_mandir}/man3/Test::XML::SAX.3*
%{_mandir}/man3/Test::XML::Twig.3*
%{_mandir}/man3/Test::XML::XPath.3*

%changelog
* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.08-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.08-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Tue Jun 06 2017 Jitka Plesnikova <jplesnik@redhat.com> - 0.08-8
- Perl 5.26 rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.08-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon May 16 2016 Jitka Plesnikova <jplesnik@redhat.com> - 0.08-6
- Perl 5.24 rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.08-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.08-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 06 2015 Jitka Plesnikova <jplesnik@redhat.com> - 0.08-3
- Perl 5.22 rebuild

* Tue Oct  7 2014 Paul Howarth <paul@city-fan.org> - 0.08-2
- BR: perl(Test::Builder::Tester) (#1148580)

* Wed Oct  1 2014 Paul Howarth <paul@city-fan.org> - 0.08-1
- Initial RPM version
